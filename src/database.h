#ifndef DATABASE_H
#define DATABASE_H

#define XTABLETAM 6
#define YTABLETAM 10
#define TAMPIECE 45
#define XSEPARATION 50
#define YSEPARATION 50
#define XINITIALPOS 250
#define YINITIALPOS 380
#define MAXMOMEMENT 15

struct circle {
	Vector2 center;
	float radius;
};

struct piece {
	Color color;
};

struct table {
	Rectangle part[XTABLETAM][YTABLETAM];
	Vector2 partPos[XTABLETAM][YTABLETAM];
	bool checkColl[XTABLETAM][YTABLETAM];
	int type[XTABLETAM][YTABLETAM];
};

struct database {
	int screenWidth = 980;
	int screenHeight = 740;
	int setFPS = 60;
	const char* tittle = "MATCH3";
	int option = 0;
	bool close = false;
	int type;
	bool selected[XTABLETAM][YTABLETAM];
	int pieceSelected = 0;
	bool pieceSelectedAct = true;
	Vector2 vectorAux;
	Vector2 PosAux;
	Vector2 ArrayPosAux;
	bool typeAct = true;
	bool eliminatedPiece[XTABLETAM][YTABLETAM];
	//---- List
	Vector2 selectedList[XTABLETAM * YTABLETAM];
	//---- Menu
	circle playButtom;
	circle exitButtom;
	Vector2 PlayDiference = { (screenWidth / 2.0f) - 65, (screenHeight / 2.0f) - 65 };
	Vector2 ExitDiference = { 2, 2 };
	//---- Table
	bool generateTableOn = true;
	table table1;
	//---- 
	piece pieceBlue = { BLUE };
	piece pieceRed = { RED };
	piece pieceGreen = { GREEN };
	piece pieceYellow = { YELLOW };
	// random variable
	int random;
	//---- Timer
	float time = 60;
	int timeRero;
	//---- gamelogic
	bool pause = false;
	bool finished = false;
	//---
	Rectangle pauseButton = { 920, 10, 50, 50 };
	Vector2 pauseButtonPos = { 920, 10 };
	//---
	Rectangle leaveButton = { 925, 80, 40, 40 };
	Vector2 leaveButtonPos = { 925, 80 };
	//---
	int movementRest = MAXMOMEMENT;
	int score;
	//TEXTURES
	Texture2D PlayButton;
	Texture2D CloseButton;
	Texture2D PauseButton;
	Texture2D HomeButton;
	Texture2D GemaAzul;
	Texture2D GemaRoja;
	Texture2D GemaVerde;
	Texture2D GemaAmarilla;
	Texture2D background;
	//---
	Sound MenuSound;
	bool MenuActive = true;
	Sound GameSound;
	bool GameActive = true;
	Sound TockSound;
};

#endif // !DATABASE_H