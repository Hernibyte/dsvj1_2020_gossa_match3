#ifndef WINDOW_H
#define WINDOW_H

#include"incl.h"

namespace win {
	void startWindow(database Database);
	void closeWindow();
}

#endif // !WINDOW_H