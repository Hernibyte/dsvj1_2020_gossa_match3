#ifndef MAINGAME_H
#define MAINGAME_H

#include"incl.h"
#include"timer.h"

namespace maingame {
	void generateTable(database& Database);
	void logicTable(database& Database);
	void checkColl(database& Database, int i, int j);
	void selecPiece(database& Database, int i, int j);
	void pieceAction(database& Database, int i, int j);
	void replacePiece(database& Database);
	void regenEliminatedPiece(database& Database);
	
	void drawTable(database Database);
	void drawPiece(Texture2D texturePiece, Vector2 pos, bool isSelected, bool isEliminated, Rectangle part, float scale);

	void logic(database& Database);
	void draw(database Database);
}

#endif // !MAINGAME_H