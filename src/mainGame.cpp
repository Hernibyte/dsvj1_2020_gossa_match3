#include"mainGame.h"

namespace maingame {
	void generateTable(database& Database) {
		if (Database.generateTableOn) {
			float posX = XINITIALPOS;
			float posY = YINITIALPOS;

			for (int i = 0; i < XTABLETAM; i++) {
				for (int j = 0; j < YTABLETAM; j++) {
					Database.random = rand() % 4;
					switch (Database.random) {
					case 0:
							Database.table1.part[i][j] = { posX, posY, TAMPIECE, TAMPIECE };
							Database.table1.partPos[i][j] = { posX, posY };
							Database.table1.type[i][j] = 0;
						break;
					case 1:
							Database.table1.part[i][j] = { posX, posY, TAMPIECE, TAMPIECE };
							Database.table1.partPos[i][j] = { posX, posY };
							Database.table1.type[i][j] = 1;
						break;
					case 2:
							Database.table1.part[i][j] = { posX, posY, TAMPIECE, TAMPIECE };
							Database.table1.partPos[i][j] = { posX, posY };
							Database.table1.type[i][j] = 2;
						break;
					case 3:
							Database.table1.part[i][j] = { posX, posY, TAMPIECE, TAMPIECE };
							Database.table1.partPos[i][j] = { posX, posY };
							Database.table1.type[i][j] = 3;
						break;
					default:
						break;
					}
					posX += XSEPARATION;
				}
				posX = XINITIALPOS;
				posY += YSEPARATION;
			}
			Database.generateTableOn = false;
		}
	}
	//------------------
	void logicTable(database& Database) {
		for (int i = 0; i < XTABLETAM; i++) {
			for (int j = 0; j < YTABLETAM; j++) {
				checkColl(Database, i, j);
				selecPiece(Database, i, j);
				pieceAction(Database, i, j);
				replacePiece(Database);
			}
		}
	}

	void checkColl(database& Database, int i, int j) {
		Database.table1.checkColl[i][j] = CheckCollisionPointRec(GetMousePosition(), Database.table1.part[i][j]);
	}

	//------------------
	void selecPiece(database& Database, int i, int j) {
		if (IsMouseButtonDown(MOUSE_LEFT_BUTTON) && Database.table1.checkColl[i][j]) {
			if (Database.typeAct) {

				Database.type = Database.table1.type[i][j];
				Database.vectorAux = { Database.table1.part[i][j].x, Database.table1.part[i][j].y };
				Database.ArrayPosAux = { (float)i, (float)j };
				Database.selected[i][j] = true;
				Database.pieceSelected++;
				//std::cout << "X: " << Database.vectorAux.x << " Y: " << Database.vectorAux.y << std::endl;
				Database.typeAct = false;

			}
			else {
				if (Database.ArrayPosAux.x == i + 1 && Database.ArrayPosAux.y == j ||
					Database.ArrayPosAux.x == i - 1 && Database.ArrayPosAux.y == j ||
					Database.ArrayPosAux.y == j + 1 && Database.ArrayPosAux.x == i ||
					Database.ArrayPosAux.y == j - 1 && Database.ArrayPosAux.x == i ||
					Database.ArrayPosAux.x == i - 1 && Database.ArrayPosAux.y == j - 1 ||
					Database.ArrayPosAux.x == i - 1 && Database.ArrayPosAux.y == j + 1 ||
					Database.ArrayPosAux.x == i + 1 && Database.ArrayPosAux.y == j - 1 ||
					Database.ArrayPosAux.x == i + 1 && Database.ArrayPosAux.y == j + 1) 
				{

					if (Database.table1.type[i][j] == Database.type && !Database.selected[i][j]) {

						Database.selected[i][j] = true;
						Database.vectorAux = { Database.table1.part[i][j].x, Database.table1.part[i][j].y };
						Database.ArrayPosAux = { (float)i, (float)j };

						if (Database.pieceSelectedAct) {

							PlaySound(Database.TockSound);
							Database.pieceSelected++;
							//std::cout << "cant piece selected: " << Database.pieceSelected << std::endl;

						}

					}

				}
			}
		}
	}

	void pieceAction(database& Database, int i, int j) {
		if (IsMouseButtonUp(MOUSE_LEFT_BUTTON)) {
			if (Database.pieceSelected < 3) {
				Database.typeAct = true;
				Database.selected[i][j] = false;
				Database.pieceSelected = 0;
			}
			else 
				if (Database.pieceSelected >= 3) {
					Database.movementRest--;
					for (int i = 0; i < XTABLETAM; i++) {
						for (int j = 0; j < YTABLETAM; j++) {
							if (Database.selected[i][j]){
								Database.eliminatedPiece[i][j] = true;
								switch (Database.table1.type[i][j]) {
								case 0:
									Database.score += 100;
									break;
								case 1:
									Database.score += 80;
									break;
								case 2:
									Database.score += 60;
									break;
								case 3:
									Database.score += 40;
									break;
								}
							}
						}
					}
					Database.pieceSelected = 0;
					Database.typeAct = true;
				}
		}
	}

	void replacePiece(database& Database) {
		for (int i = 0; i < (XTABLETAM - 1); i++) {
			for (int j = 0; j < YTABLETAM; j++) {
				if (Database.eliminatedPiece[i + 1][j] && !Database.eliminatedPiece[i][j]) {
					Database.table1.type[i + 1][j] = Database.table1.type[i][j];
					Database.eliminatedPiece[i + 1][j] = false;
					Database.eliminatedPiece[i][j] = true;
				}
			}
		}
	}

	void  regenEliminatedPiece(database& Database) {
		for (int i = 0; i < (XTABLETAM); i++) {
			for (int j = 0; j < YTABLETAM; j++) {
				if (Database.eliminatedPiece[i][j]) {
					Database.random = rand() % 4;
					Database.selected[i][j] = false;
					Database.eliminatedPiece[i][j] = false;
					Database.table1.type[i][j] = Database.random;
				}
			}
		}
	}


	//------------------
	void drawTable(database Database) {
		//---
		for (int i = 0; i < XTABLETAM; i++) {
			for (int j = 0; j < YTABLETAM; j++) {
				switch (Database.table1.type[i][j]) {
				case 0:
					drawPiece(Database.GemaAzul, 
						Database.table1.partPos[i][j], 
						Database.selected[i][j], 
						Database.eliminatedPiece[i][j], 
						Database.table1.part[i][j],
						0.05f);
				
					break;
				case 1:
					drawPiece(Database.GemaRoja,
						Database.table1.partPos[i][j],
						Database.selected[i][j],
						Database.eliminatedPiece[i][j],
						Database.table1.part[i][j],
						0.05f);
						
					break;
				case 2:
					drawPiece(Database.GemaVerde,
						Database.table1.partPos[i][j],
						Database.selected[i][j],
						Database.eliminatedPiece[i][j],
						Database.table1.part[i][j],
						0.05f);

					break;
				case 3:
					Database.PosAux = { Database.table1.part[i][j].x - 8, Database.table1.part[i][j].y - 7 };
					drawPiece(Database.GemaAmarilla,
						Database.PosAux,
						Database.selected[i][j],
						Database.eliminatedPiece[i][j],
						Database.table1.part[i][j],
						0.09f);

					break;
				default:
					break;
				}
			}
		}
		//---
	}

	void drawPiece(Texture2D texturePiece, Vector2 pos, bool isSelected, bool isEliminated, Rectangle part, float scale) {
		DrawTextureEx(texturePiece, pos, 0.0f, scale, WHITE);

		if (isSelected == true) {
			DrawTextureEx(texturePiece, pos, 0.0f, scale, VIOLET);
		}
		else
			if (isEliminated == true) {
				DrawRectangleRec(part, BLACK);
			}
	}

	//------------------
	void logic(database& Database) {

		if (Database.GameActive) {
			PlaySound(Database.GameSound);
			Database.GameActive = false;
		}
		if (!IsSoundPlaying(Database.GameSound)) {
			Database.GameActive = true;
		}

		bool checkPauseCollision = CheckCollisionPointRec(GetMousePosition(), Database.pauseButton);
		bool checkLeaveCollision = CheckCollisionPointRec(GetMousePosition(), Database.leaveButton);

		if (Database.movementRest == 0) {
			Database.finished = true;
		}

		if (checkPauseCollision && !Database.finished) {
			if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
				Database.pause = !Database.pause;
			}
		}

		if (!Database.pause && !Database.finished) {
			if (!IsSoundPlaying(Database.GameSound)) {
				ResumeSound(Database.GameSound);
			}

			generateTable(Database);
			logicTable(Database);
			regenEliminatedPiece(Database);
		}
		else 
			if (Database.pause && !Database.finished) {
				if (IsSoundPlaying(Database.GameSound)) {
					PauseSound(Database.GameSound);
				}
				//---
				if (checkLeaveCollision) {
					if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
						//---
						Database.option = 0;
						Database.pieceSelected = 0;
						Database.pause = false;
						Database.generateTableOn = true;
						Database.finished = false;
						Database.movementRest = MAXMOMEMENT;
						Database.score = 0;
						//---
					}
				}
				//---
			}

		if (Database.finished) {
			StopSound(Database.GameSound);
			if (IsKeyPressed(KEY_ENTER)) {
				//---
				Database.option = 0;
				Database.pieceSelected = 0;
				Database.pause = false;
				Database.generateTableOn = true;
				Database.finished = false;
				Database.movementRest = MAXMOMEMENT;
				Database.score = 0;
				//---
			}
		}
	}

	//------------------
	void draw(database Database) {
		DrawTexture(Database.background, 0, 0, WHITE);
		//---
		if (Database.finished) {
			DrawText("Ooops te quedaste sin movimientos", 140, 100, 40, WHITE);
			DrawText(FormatText("Puntaje: %i", Database.score), 340, 300, 50, WHITE);
			DrawText("Press ENTER to continue", 50, 700, 30, WHITE);
		}
		else {
			DrawTextureEx(Database.PauseButton, Database.pauseButtonPos, 0.0f, 0.08f, WHITE);
			DrawText("MOVIMIENTOS:", 360, 150, 38, WHITE);
			DrawText(FormatText("%i", Database.movementRest), 470, 200, 38, WHITE);
			DrawText("PUNTAJE:", 410, 250, 38, WHITE);
			DrawText(FormatText("%i", Database.score), 450, 300, 38, WHITE);

			if (Database.pause) {
				DrawTextureEx(Database.HomeButton, Database.leaveButtonPos, 0.0f, 0.08f, WHITE);
				DrawText("PAUSE", 400, 300, 60, BLACK);
			}
			//---
			drawTable(Database);
		}
		//---
	}
}