#include"mainloop.h"

namespace mainloop {
	void loop(database& Database) {
		while (!WindowShouldClose() && !Database.close) {
			logic::process(Database);
			draw::process(Database);
		}
	}
}