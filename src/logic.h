#ifndef LOGIC_H
#define LOGIC_H

#include"incl.h"
#include"mainMenu.h"
#include"mainGame.h"

namespace logic {
	void process(database& Database);
}

#endif // !LOGC_H