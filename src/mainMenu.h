#ifndef MAINMENU_H
#define MAINMENU_H

#include"incl.h"

namespace mainmenu {
	void logic(database& Database);
	void draw(database Database);
}

#endif // !MAINMENU_H