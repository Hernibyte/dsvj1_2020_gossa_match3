#ifndef MAINLOOP_H
#define MAINLOOP_H

#include"incl.h"
#include"logic.h"
#include"draw.h"

namespace mainloop {
	void loop(database& Database);
}

#endif // MAINLOOP_H