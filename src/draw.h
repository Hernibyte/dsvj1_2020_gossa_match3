#ifndef DRAW_H
#define DRAW_H

#include"incl.h"
#include"mainMenu.h"
#include"mainGame.h"

namespace draw {
	void process(database Database);
}

#endif // !DRAW_H