#include"basegame.h"

namespace basegame {
	database Database;
	void startGame() {
		win::startWindow(Database);
		LoadTextures();
		LoadMusic();
		mainloop::loop(Database);
		DeleteTextures();
		DeleteMusic();
		win::closeWindow();
	}

	void LoadTextures() {
		Database.PlayButton = LoadTexture("../res/assets/Play.png");
		Database.CloseButton = LoadTexture("../res/assets/Close.png");
		Database.PauseButton = LoadTexture("../res/assets/Pause.png");
		Database.HomeButton = LoadTexture("../res/assets/Home.png");
		Database.GemaAzul = LoadTexture("../res/assets/GemaAzul.png");
		Database.GemaRoja = LoadTexture("../res/assets/GemaRoja.png");
		Database.GemaVerde = LoadTexture("../res/assets/GemaVerde.png");
		Database.GemaAmarilla = LoadTexture("../res/assets/GemaAmarilla.png");
		Database.background = LoadTexture("../res/assets/background-p.png");
	}

	void DeleteTextures() {
		UnloadTexture(Database.PlayButton);
		UnloadTexture(Database.CloseButton);
		UnloadTexture(Database.PauseButton);
		UnloadTexture(Database.HomeButton);
		UnloadTexture(Database.GemaAzul);
		UnloadTexture(Database.GemaRoja);
		UnloadTexture(Database.GemaVerde);
		UnloadTexture(Database.GemaAmarilla);
		UnloadTexture(Database.background);
	}

	void LoadMusic() {
		InitAudioDevice();
		//---
		Database.MenuSound = LoadSound("../res/assets/Menu.mp3");
		Database.GameSound = LoadSound("../res/assets/Game.mp3");
		Database.TockSound = LoadSound("../res/assets/tock.mp3");
	}

	void DeleteMusic() {
		UnloadSound(Database.MenuSound);
		UnloadSound(Database.GameSound);
		UnloadSound(Database.TockSound);
		//---
		CloseAudioDevice();
	}

}