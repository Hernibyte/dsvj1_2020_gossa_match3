#include"mainMenu.h"

namespace mainmenu {
	void logic(database& Database) {

		if (Database.MenuActive) {
			PlaySound(Database.MenuSound);
			Database.MenuActive = false;
		}
		if (!IsSoundPlaying(Database.MenuSound)) {
			Database.MenuActive = true;
		}

		Database.playButtom = {Database.screenWidth / 2.0f, Database.screenHeight / 2.0f, 60.0f};
		Database.exitButtom = { 30, 30, 25.0f };

		bool checkPlay = false;
		bool checkExit = false;

		checkPlay = 
			CheckCollisionPointCircle(GetMousePosition(), Database.playButtom.center, 
									  Database.playButtom.radius);

		checkExit =
			CheckCollisionPointCircle(GetMousePosition(), Database.exitButtom.center, 
									  Database.exitButtom.radius);

		if (checkPlay) {
			if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
				Database.option = 1;
				StopSound(Database.MenuSound);
			}
		}
		if (checkExit) {
			if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
				Database.close = true;
			}
		}
	}
	void draw(database Database) {
		DrawTexture(Database.background, 0, 0, WHITE);
		DrawText("MATCH3", 330, 60, 80, WHITE);
		DrawTextureEx(Database.PlayButton, Database.PlayDiference, 0.0f, 0.25f, WHITE);
		DrawTextureEx(Database.CloseButton, Database.ExitDiference, 0.0f, 0.11f, WHITE);
	}
}