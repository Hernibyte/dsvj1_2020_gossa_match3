#ifndef BASEGAME_H
#define BASEGAME_H

#include"incl.h"
#include"window.h"
#include"mainloop.h"

namespace basegame {
	void startGame();
	void LoadTextures();
	void DeleteTextures();
	void LoadMusic();
	void DeleteMusic();
}

#endif // !BASEGAME_H